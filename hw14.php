	
<?php
//открываем файл для записи
$finfo_list = fopen('ListInfo.csv', 'w');
$dir = 'img/';


// проверяем является ли искомый путь каталогом
if (is_dir($dir)) {
	//открываем каталог
	if ($dh = opendir($dir)) {
    	//обходим все содержимое каталога
		while (($file = readdir($dh)) !== false) {
        	//если это файл то выполняем нужные действия		
			if (is_file($dir.'/'.$file) == true) {
				$stat = stat($dir.'/'.$file);
				echo "файл: $file <br>";
				echo 'Последнее время изменения: ' . date("Y-m-d H:i:s",$stat['ctime']).' Размер '.$stat['size'] . '<br>';
				
				$file_info = pathinfo($dir.'/'.$file);
				list($width, $height) = getimagesize($dir.'/'.$file);
				$new_width = $width * 0.2;
				$new_height = $height * 0.2;
				//ресэмплирование
				$image_p = imagecreatetruecolor($new_width, $new_height);
				$image = imagecreatefromjpeg($dir.'/'.$file);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				// вывод в файл в папке prev
				imagejpeg($image_p, $dir.'prev/'.$file_info['filename'].'_small.'.$file_info['extension'], 100);
				//сохраняем информацию в csv имяФайла; имяпревью; датамодификации;размер
				fputcsv($finfo_list, array($file,$dir.'prev/'.$file_info['filename'].'_small.'.$file_info['extension'],$stat['ctime'],$stat['size']),';');
			}
		}
      //закрываем ресурс каталога  
		closedir($dh);
	}
}
fclose($finfo_list);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>ДЗ 1.4 Стандартные функции</title>
	<style type="text/css">
		div {
			display: table;
		}
		ul {
			display: table-row;
		}
		li {
			display: table-cell;
			padding: 0 5px;
			vertical-align: middle;
		}
	</style>
</head>
<body>
<?php
if (($finfo_list = fopen("ListInfo.csv", "r")) !== FALSE) {
	 while (($data = fgetcsv($finfo_list, 0, ";")) !== FALSE) {
?><ul>
		<li><img src="<?= $data[1]?>"></img></li>
		<li><a href="<?= $dir.$data[0]?>"><?= $dir.$data[0]?></a></li>
		<li><?= date("Y-m-d H:i:s",$data[2]) ?></li>
		<li><?= $data[3] ?></li>
	</ul>	
<?php	
	 }
  fclose($finfo_list);
}
?>